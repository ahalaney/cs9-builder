# cs9-builder

Containers for building the centos-stream-9 kernel.

## Description
There are two Dockerfiles provided here, one for a fedora container
(`Dockerfile.f35`) and one for a CentOS Stream 9 container.

The fedora container is useful if you want to cross compile kernels the
manual way. It allows you to generate Red Hat configs and then use those
to `make Image.gz`, etc. Currently only the aarch64 cross compiler is
installed by default

The centos-stream-9 container is intended to allow you to actually build
RPMs locally for your native arch (i.e. `make dist-all-rpms`).

## Usage
The builder is simple to use and should work fine with docker or podman. I use
podman here, but substitute the commands with docker instead if that's your thing.

Build the centos-stream-9 image and name it cs9-builder:
```
podman build -t cs9-builder -f ./Dockerfile.cs9 .
```
or build the fedora image and name it cs9-builder:
```
podman build -t cs9-builder -f ./Dockerfile.f35 .
```

Run the image, requesting it to be interactive, with a tty,
mounting `~/git/redhat/centos-stream-9` of the host into the
container at `/centos-stream-9`, relabelling as necessary (the `:z`)
for selinux, and running bash in the container:
```
podman run -it -v ~/git/redhat/centos-stream-9:/centos-stream-9:z cs9-builder /bin/bash
```

Build the kernel RPMs for fun if you're using the centos-stream-9 image:
```
cd /centos-stream-9
git checkout main
make dist-all-rpms
```
The resulting rpms should be displayed at the end and are accessible outside
the container as well.

Or use standard kernel makefile targets with the fedora image and cross compile
to aarch64:
```
cd /centos-stream-9
git checkout main
make dist-configs
cp /centos-stream-9/redhat/configs/kernel-5.14.0-aarch64.config .config
make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- -j`nproc` Image.gz dtbs modules
```
Once again, the resulting artifacts are available to the host for further
usage.
